
CREATE DATABASE GameStore;
GO

USE GameStore;
GO

CREATE TABLE Publishers
(
    Id_publisher INT IDENTITY (1,1) PRIMARY KEY,
    Name         NVARCHAR(50),
    Address      NVARCHAR(50)
);
GO

CREATE TABLE Customers
(
    Id_customer  INT IDENTITY (1,1) PRIMARY KEY,
    Id_publisher INT NULL,
    FOREIGN KEY (Id_publisher) REFERENCES Publishers (Id_publisher),
    UserName     NVARCHAR(50),
    LastName     NVARCHAR(50),
    FirstName    NVARCHAR(50),
    Email        NVARCHAR(100),
    PhoneNumber  NVARCHAR(15),
    RatingScores INT
);
GO

CREATE TABLE Games
(
    Id_game      INT IDENTITY (1,1) PRIMARY KEY,
    UserId       INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Customers (Id_customer) ON DELETE CASCADE,
    Title        NVARCHAR(MAX) NOT NULL,
    ReleaseDate  DATETIME2 NOT NULL DEFAULT GETDATE(),
    Genre        NVARCHAR(50) NOT NULL,
    Price        DECIMAL(10,2) NOT NULL,
    Description  NVARCHAR(MAX) NOT NULL
);
GO

CREATE TABLE Reviews
(
    Id_review    INT IDENTITY (1,1) PRIMARY KEY,
    GameId       INT NOT NULL,
    FOREIGN KEY (GameId) REFERENCES Games (Id_game) ON DELETE CASCADE,
    UserId       INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Customers (Id_customer) ON DELETE NO ACTION,
    Rating       INT NOT NULL CHECK (Rating BETWEEN 1 AND 5),
    DateCreate   DATETIME2 NOT NULL DEFAULT GETDATE(),
    Content      NVARCHAR(MAX) NOT NULL
);
GO

CREATE TABLE GameCategories
(
    Id_category INT IDENTITY (1,1) PRIMARY KEY,
    Name        NVARCHAR(50) NOT NULL
);
GO

CREATE TABLE GameCategoryMapping
(
    GameId     INT NOT NULL,
    FOREIGN KEY (GameId) REFERENCES Games (Id_game) ON DELETE CASCADE,
    CategoryId INT NOT NULL,
    FOREIGN KEY (CategoryId) REFERENCES GameCategories (Id_category) ON DELETE CASCADE,
    CONSTRAINT [PK_GameCategoryMapping] PRIMARY KEY CLUSTERED ([GameId], [CategoryId])
);
GO

-- ????????? ?? ??????? Publishers
INSERT INTO Publishers (Name, Address)
VALUES
    ('Ubisoft', '123 Game St.'),
    ('EA', '456 Gaming Ave.'),
    ('Nintendo', '789 Gaming Blvd.'),
    ('Activision', '101 Game Lane.');

-- ????????? ?? ??????? Customers
INSERT INTO Customers (Id_publisher, UserName, LastName, FirstName, Email, PhoneNumber, RatingScores)
VALUES
    (1, 'gamer1', 'Doe', 'John', 'john.doe@email.com', '(123)4567890', 5),
    (2, 'player2', 'Smith', 'Alice', 'alice.smith@email.com', '(123)4567432', 4),
    (1, 'gamer3', 'Johnson', 'Emily', 'emily.johnson@email.com', '(123)4567012', 3),
    (2, 'player4', 'Anderson', 'Michael', 'michael.anderson@email.com', '(123)4567654', 4);

-- ????????? ?? ??????? Games
INSERT INTO Games (UserId, Title, Genre, Price, Description)
VALUES
    (1, 'Assassin''s Creed Valhalla', 'Action-Adventure', 59.99, 'Explore the Viking age as Eivor, a fierce Viking warrior.'),
    (2, 'The Legend of Zelda: Breath of the Wild', 'Action-Adventure', 49.99, 'Embark on a journey in the vast world of Hyrule.'),
    (1, 'Call of Duty: Warzone', 'Shooter', 0, 'Battle royale mode with various multiplayer options.'),
    (3, 'Cyberpunk 2077', 'Role-Playing', 49.99, 'Navigate the futuristic world of Night City and its dark secrets.');

-- ????????? ?? ??????? Reviews
INSERT INTO Reviews (GameId, UserId, Rating, Content)
VALUES
    (1, 2, 4, 'Great game! Enjoyed the storyline and gameplay.'),
    (2, 1, 5, 'Amazing game! Stunning graphics and an immersive world.'),
    (1, 3, 3, 'Good game, but I expected more challenges.'),
    (4, 4, 4, 'Exciting RPG experience with a captivating storyline.');

-- ????????? ?? ??????? GameCategories
INSERT INTO GameCategories (Name)
VALUES
    ('Action-Adventure'),
    ('Shooter'),
    ('Role-Playing'),
    ('Simulation');

-- ????????? ?? ??????? GameCategoryMapping
INSERT INTO GameCategoryMapping (GameId, CategoryId)
VALUES
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4);
-- Create Logins
CREATE LOGIN gamestore_admin WITH PASSWORD = 'adminpass';
CREATE LOGIN gamestore_user WITH PASSWORD = 'userpass';

-- Switch to GameStore Database
USE GameStore;

-- Create Users
CREATE USER gamestore_administrator FOR LOGIN gamestore_admin;
CREATE USER gamestore_customer FOR LOGIN gamestore_user;

-- Assign Roles
ALTER ROLE db_owner ADD MEMBER gamestore_administrator;

-- Grant Permissions
GRANT SELECT, INSERT, UPDATE, DELETE ON Publishers TO gamestore_administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO gamestore_administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON Games TO gamestore_administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON Reviews TO gamestore_administrator;
GRANT SELECT ON GameCategories TO gamestore_administrator;
GRANT SELECT ON GameCategoryMapping TO gamestore_administrator;

-- Grant permissions for gamestore_customer
GRANT SELECT ON Publishers TO gamestore_customer;
GRANT SELECT, INSERT, UPDATE ON Customers TO gamestore_customer;
GRANT SELECT, INSERT, UPDATE ON Games TO gamestore_customer;
GRANT SELECT, INSERT, UPDATE ON Reviews TO gamestore_customer;
GRANT SELECT ON GameCategories TO gamestore_customer;
GRANT SELECT ON GameCategoryMapping TO gamestore_customer;

-- Create Symmetric Key for Encryption
CREATE SYMMETRIC KEY GameStoreSymmetricKey
WITH ALGORITHM = AES_256
ENCRYPTION BY PASSWORD = '1234567123';

-- Create Trigger for Encryption
CREATE TRIGGER EncryptGameDetails
ON Games
AFTER INSERT, UPDATE
AS
BEGIN
    SET NOCOUNT ON;
    UPDATE Games
    SET Title = ENCRYPTBYKEY(KEY_GUID('GameStoreSymmetricKey'), inserted.Title),
        Description = ENCRYPTBYKEY(KEY_GUID('GameStoreSymmetricKey'), inserted.Description)
    FROM Games
    INNER JOIN inserted ON Games.Id_game = inserted.Id_game;
END;

-- Create Procedure for Decryption
CREATE PROCEDURE DecryptGameDetails
AS
BEGIN
    SELECT Id_game, Title,
        CONVERT(varchar(max), DECRYPTBYKEY(Title)) AS Decrypted_Title,
        CONVERT(varchar(max), DECRYPTBYKEY(Description)) AS Decrypted_Description
    FROM Games;
END;

